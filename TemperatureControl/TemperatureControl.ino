#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include <microDS18B20.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <cppQueue.h>

ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

#define OTAUSER         "admin"               // Логин для входа в OTA
#define OTAPASSWORD     "tempUpdate"          // Пароль для входа в ОТА
#define OTAPATH         "/firmware"           // Путь, который будем дописывать после ip адреса в браузере.

#define MODEM_RST            4
#define GsmModem Serial
#define Debug Serial1

#define power_check 14
bool power = true;

#include <sim800_sms.h>
#define MESSAGE_LENGTH  100
char gprsBuffer[64];
char *s = NULL;
int inComing = 0;

char* tel1 = "+380505384922";

#define BOTtoken "1837771022:AAE9EErVnQj6yqI7m_mU9qi9U2J92LrY2f0"

const unsigned long BOT_MTBS = 1000;  // mean time between scan messages

unsigned long bot_lasttime;            // last time messages' scan has been done
X509List cert(TELEGRAM_CERTIFICATE_ROOT);
WiFiClientSecure secured_client;
UniversalTelegramBot bot(BOTtoken, secured_client);

const char* admin_pass = "adminsuperpass";
bool admin_access = false;

MicroDS18B20<5> sensor_1;
unsigned long temp_read_lasttime = 0;
float max_temp_room = 27;
float min_temp_room = 26;
bool alarm_temperature = false;

const char* ssid     = "Guest WLAN";
const char* password = "11111111";

String group_chat_id = "-517410913";
String admin_chat_id = "404448933";

cppQueue  bot_message_queue(sizeof(String), 50, FIFO, false);

void setup(void){
  
  pinMode(MODEM_RST, OUTPUT);
  digitalWrite(MODEM_RST, HIGH);

  pinMode(power_check, INPUT);
  digitalWrite(power_check, LOW);

  GsmModem.begin(9600);
  GsmModem.swap();
  Debug.begin(9600);
  Debug.setDebugOutput(true);

  EEPROM.begin(512);

  configTime(0, 0, "pool.ntp.org");         // get UTC time via NTP
  secured_client.setTrustAnchors(&cert);    // Add root certificate for api.telegram.org

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED){
    delay(5000);
    Debug.print(".");
  }

  Debug.println("");
  Debug.println("WiFi connected");
  Debug.println("IP address: ");
  Debug.println(WiFi.localIP());

   // Check NTP/Time, usually it is instantaneous and you can delete the code below.
  Debug.print("Retrieving time: ");
  time_t now = time(nullptr);
  while (now < 24 * 3600){
    Debug.print(".");
    delay(100);
    now = time(nullptr);
  }
  Debug.println(now);

  Debug.println("Modem start!");
  digitalWrite(MODEM_RST, LOW);
  delay(6000);
  
  // sendSMS("+380505384922", "testSMS");

  httpUpdater.setup(&server, OTAPATH, OTAUSER, OTAPASSWORD);
  server.begin();
}

void loop(void){
  
  if(GsmModem.available()){
    inComing = 1;
    }
    else delay(100);
   
  if(inComing){
    
      readBuffer(gprsBuffer,32,DEFAULT_TIMEOUT);
      Debug.println(gprsBuffer);
 
      if(NULL != strstr(gprsBuffer,"RING")) {
      //answer();
      call_down();
      }
      
      else if(NULL != (s = strstr(gprsBuffer,"+CMTI: \"SM\""))){       //SMS: $$+CMTI: "SM",24$$
        char message[MESSAGE_LENGTH];
        int messageIndex = atoi(s+12);
        readSMS(messageIndex, message, MESSAGE_LENGTH);
        Debug.println(message);
        char c = messageIndex + '0';   //convert to char

        Debug.print("AT+CMGD="); 
        Debug.println(c); 
        GsmModem.print("AT+CMGD=");   // delete message
        GsmModem.println(c);
      }
      cleanBuffer(gprsBuffer,32);
      inComing = 0;
  }

  if(millis() - bot_lasttime > BOT_MTBS){
    int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    while (numNewMessages){
      Debug.println("got response");
      handleNewMessages(numNewMessages);
      numNewMessages = bot.getUpdates(bot.last_message_received + 1);
    }
    bot_lasttime = millis();
  }

  if(millis() - temp_read_lasttime > 60000){
   
      Debug.print("Room temperature: ");
      Debug.println(sensor_1.getTemp());
      if(sensor_1.getTemp() >= max_temp_room && !alarm_temperature){
        bot.sendMessage(group_chat_id, "ТРИВОГА!!!:\n", "Markdown");
        bot.sendMessage(group_chat_id, "Критична температура: " + (String)sensor_1.getTemp() + " ℃", "");
       // String botSendMessage = "Критична температура: " + (String)sensor_1.getTemp() + " ℃";
       // bot_message_queue.push(&botSendMessage);

        char smsMessage[21] = {'s','2',':','A','C','H','T','U','N','G','!',' ','T','e','m','p','.',' '};
        char char_temp[3];
        dtostrf(sensor_1.getTemp(), 3, 1, char_temp);
        strncat(smsMessage, char_temp, 5);
        Debug.println(smsMessage);
        
        sendSMS("+380505384922", smsMessage);

        delay(500);
        alarm_temperature = true;
        Debug.println("alarm_temperature = true");
      }
      if(sensor_1.getTemp() <= min_temp_room && alarm_temperature){
        alarm_temperature = false;
        Debug.println("alarm_temperature = false");
      }
      temp_read_lasttime = millis();
  }

  if(!digitalRead(power_check) && power){
    bot.sendMessage(group_chat_id, "Живлення відсутнє!\n", "Markdown");
    power = false;
  }

   if(digitalRead(power_check) && !power){
    bot.sendMessage(group_chat_id, "Живлення відновлено!\n", "Markdown");
    power = true;
  }
/*
  if(WiFi.status() == WL_CONNECTED){
    
    while (bot_message_queue.getCount()>0) {
      Debug.println("bot_pop");
      String message;
      bot_message_queue.pop(&message);
      bot.sendMessage(admin_chat_id, message, "Markdown");      
    }
  }*/

  sensor_1.requestTemp();

  server.handleClient();
}


void handleNewMessages(int numNewMessages){
  
  Debug.print("handleNewMessages ");
  Debug.println(numNewMessages);
  
for (int i = 0; i < numNewMessages; i++){
    bool user_access = false;
    String chat_id = bot.messages[i].chat_id;
    String text = bot.messages[i].text;

    String from_name = bot.messages[i].from_name;
    if(from_name == "") from_name = "Guest";

    if(text == admin_pass){
      admin_access = true;
      bot.sendMessage(chat_id, "Пароль прийнято!\n", "Markdown");
      bot.sendMessage(chat_id, "Налаштування:\n", "Markdown");
      bot.sendMessage(chat_id, "/clearallusers : Очистити список користувачів.\n", "Markdown");
    }
        
    if(chat_id == group_chat_id || chat_id == admin_chat_id){
      
      if (text == "/start"){
        String welcome = "Ласкаво просимо, " + from_name + ".\n";
        welcome += "/getSRVroomTemp : Отримати поточну температуру.\n";
        bot.sendMessage(chat_id, welcome, "Markdown");
      }
      if (text == "/getSRVroomTemp" || text == "/getSRVroomTemp@room_temperature_bot"){
        bot.sendMessage(chat_id, "Температура повітря: " + (String)sensor_1.getTemp() + " ℃", "");
      }
      if(chat_id == admin_chat_id){
        if (text == "/setAdminConfig" && admin_access){
          bot.sendMessage(chat_id, "Налаштування:\n", "Markdown");
          bot.sendMessage(chat_id, "/addTelNumber : Додати номер до розсилки.\n", "Markdown");
        }
        else if (text == "/setAdminConfig" && !admin_access){
          bot.sendMessage(chat_id, "Введіть пароль адміністратора:\n", "Markdown");
        }
        if (text == "/addTelNumber" && admin_access){
        /*  for(int i = 1; i <= EEPROM.read(0)+1; i++){
            EEPROM.put(i, 0);
          }
          EEPROM.commit();
          EEPROM.write(0, 0);
          bot.sendMessage(chat_id, "Список користувачів очищено", "Markdown");*/
          admin_access = false;
        }
      }
    }
    else{
      bot.sendMessage(chat_id, "No access, Chat ID: " + chat_id, "Markdown");
    }
  }
}
