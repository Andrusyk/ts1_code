#define DEFAULT_TIMEOUT     5

#define UART_DEBUG

#ifdef UART_DEBUG
#define ERROR(x)            Debug.println(x)
#define DEBUG(x)            Debug.println(x);
#else
#define ERROR(x)
#define DEBUG(x)
#endif

int sendSMS(char*, char*);
int sendCmdAndWaitForResp(const char*, const char*, unsigned);
void sendCmd(const char*);
void sendEndMark(void);
int waitForResp(const char*, unsigned int);
void cleanBuffer(char*, int);
int readBuffer(char*, int, unsigned int);
int answer(void);
int call_down(void);

/********************** Send SMS **********************************/

int sendSMS(char *number, char *data){
    char cmd[32];
    if(0 != sendCmdAndWaitForResp("AT+CMGF=1\r\n", "OK", DEFAULT_TIMEOUT)) { // Set message mode to ASCII
        ERROR("ERROR:CMGF");
        return -1;
    }
    delay(500);
    snprintf(cmd, sizeof(cmd),"AT+CMGS=\"%s\"\r\n", number);
    if(0 != sendCmdAndWaitForResp(cmd,">",DEFAULT_TIMEOUT)){
        ERROR("ERROR:CMGS");
        return -1;
    }
    delay(1000);
    GsmModem.write(data);
    delay(500);
    sendEndMark();
    return 0;
}

int sendCmdAndWaitForResp(const char* cmd, const char *resp, unsigned timeout){
    sendCmd(cmd);
    return waitForResp(resp,timeout);
}

void sendCmd(const char* cmd){
    GsmModem.write(cmd);
}

void sendEndMark(void){
    GsmModem.println((char)26);
}

int waitForResp(const char *resp, unsigned int timeout){
    int len = strlen(resp);
    int sum=0;
    unsigned long timerStart,timerEnd;
    timerStart = millis();
    
    while(1){
        if(GsmModem.available()){
            char c = GsmModem.read();
            sum = (c==resp[sum]) ? sum+1 : 0;
            if(sum == len)break;
        }
        timerEnd = millis();
        if(timerEnd - timerStart > 1000 * timeout){
            return -1;
        }
    }

    while(GsmModem.available()){
        GsmModem.read();
    }
    return 0;
}

/******************* Read SMS ****************************/

int readSMS(int messageIndex, char *message, int length){
    int i = 0;
    char gprsBuffer[144]; // Buffer size for the SMS message
    char cmd[16];
    char *p,*s;

    sendCmdAndWaitForResp("AT+CMGF=1\r\n","OK",DEFAULT_TIMEOUT);
    delay(1000);
    sprintf(cmd,"AT+CMGR=%d\r\n",messageIndex);
    GsmModem.write(cmd);
    cleanBuffer(gprsBuffer,144);
    readBuffer(gprsBuffer,144,DEFAULT_TIMEOUT);

    if(NULL != ( s = strstr(gprsBuffer,"+CMGR"))){
        if(NULL != ( s = strstr(gprsBuffer,"REC"))){  // Search the beginning of the SMS message
            p = s - 1;
            while((i < length-1)) {
                message[i++] = *(p++);
            }
            message[i] = '\0';
        }
    }
    return 0;
}

void cleanBuffer(char *buffer, int count){
    for(int i=0; i < count; i++) {
        buffer[i] = '\0';
    }
}

int readBuffer(char *buffer,int count, unsigned int timeOut){
    int i = 0;
    unsigned long timerStart,timerEnd;
    timerStart = millis();
    while(1) {
        while (GsmModem.available()) {
            char c = GsmModem.read();
            if (c == '\r' || c == '\n') c = '$';                            
            buffer[i++] = c;
            if(i > count-1)break;
        }
        if(i > count-1)break;
        timerEnd = millis();
        if(timerEnd - timerStart > 1000 * timeOut) {
            break;
        }
    }
    delay(500);
    while(GsmModem.available()) {   // display the other thing..
        GsmModem.read();
    }
    return 0;
}

int answer(void){
    GsmModem.write("ATA\r\n");
    return 0;
}

int call_down(void){
    GsmModem.write("ATH0\r\n");
    return 0;
}
